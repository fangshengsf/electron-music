import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        component: () => import('@renderer/view/home/index.vue'),
        redirect: '/findMusic',
        children: [
            {
                path: '/findMusic',
                component: () => import('@renderer/view/home/findMusic/index.vue'),
            },
            {
                path: '/everyMusic',
                component: () => import('@renderer/view/home/findMusic/edrMusic.vue')
            },
            {
                path: '/bolg',
                component: () => import('@renderer/view/home/blog/index.vue')
            },
            {
                path: '/searchMusic',
                name: 'searchMusic',
                component: () => import('@renderer/view/home/findMusic/searchMusic.vue')
            },
            {
                path: '/songListDetail',
                component: () => import('@renderer/view/home/findMusic/songListDetail.vue')
            },
            {
                path: '/mv',
                name: 'mv',
                component: () => import('@renderer/view/home/findMusic/mvDetail.vue')
            },
            {
                path: '/singerInfo',
                name: 'singerInfo',
                component: () => import('@renderer/view/home/findMusic/singerInfo.vue')
            },
            {
                path: '/albumInfo',
                name: 'albumInfo',
                component: () => import('@renderer/view/home/findMusic/albumInfo.vue')
            }
        ]
    }
]
const router = createRouter({
    history: createWebHashHistory(),
    routes,
})
export default router