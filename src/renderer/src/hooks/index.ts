// 百万播放量计数
export const setVNumber = (number: number) => {
    if (number >= 100000000) {
        return Math.floor(number / 100000000) + '亿'
    } else if (number >= 10000) {
        return Math.floor((number / 10000)) + '万'
    } else {
        return number
    }
}
// 根据当前时间设置问候好
export const setTime = () => {
    let timer = new Date().getHours()
    const greetings = ['凌晨好', '早上好', '下午好', '晚上好'];
    if (timer == 12) {
        return '中午好'
    } else {
        return greetings[Math.floor(timer / 6)];
    }
}

// 根据歌曲时间戳，计算歌曲时长
export const setDuration = (duration: number) => {
    const min = Math.floor(duration / 1000 / 60);
    const sec = Math.floor(duration / 1000 % 60);
    if (min < 1) {
        return `${sec < 10 ? "00:" + '0' + sec : "00:" + sec}`
    }
    return `${min < 10 ? '0' + min : min}:${sec < 10 ? '0' + sec : sec}`
}
// 根据歌曲时间戳，统计歌曲秒数
export const sumTime = (duration: number) => {
    const min = Math.floor(duration / 1000 / 60);
    const sec = Math.floor(duration / 1000 % 60);
    const num = min * 60 + sec
    return num
}
// 根据歌曲时间秒数，计算歌曲时长
export const songTime = (duration: number) => {
    const min = Math.floor(duration / 60);
    const sec = Math.floor(duration % 60);
    if (min < 1) {
        return `${sec < 10 ? '0' + sec : sec}`
    }
    return `${min < 10 ? '0' + min : min}:${sec < 10 ? '0' + sec : sec}`
}
export const timestampToDate = (timestamp: number) => {
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return `${year}年-${month}月-${day}日`;
}