export type mvType = {
    "id": number,
    "url": string,
    "r": number,
    "size": number,
    "md5": string,
    "code": number,
    "expi": number,
    "fee": number,
    "mvFee": number,
    "st": number,
    "promotionVo": null,
    "msg": string
}
export type mvInfoTyoe = {
    "id": number,
    "name": string,
    "artistId": number,
    "artistName": string,
    "desc": string,
    "cover": string,
    "playCount": number,
    "subCount": number,
    "shareCount": number,
    "commentCount": number,
    "duration": number,
    "publishTime": string,
    "price": number | null,
    "brs": Array<{
        "size": number,
        "br": number,
        "point": number
    }>,
    "artists": Array<
        {
            "id": number,
            "name": string,
            "img1v1Url": string,
            "followed": boolean
        }
    >,
    "videoGroup": Array<{
        "id": number,
        "name": string,
        "type": number
    }>
}
export type mvCountType = {
    "likedCount": number,
    "shareCount": number,
    "commentCount": number,
    "liked": boolean,
}
type MvCommentListType = {
    beReplied: any[];
    commentId: number;
    commentLocationType: number;
    content: string;
    contentResource: null;
    decoration: {};
    expressionUrl: null;
    grade: null;
    ipLocation: {
        ip: null;
        location: string;
        userId: number;
    };
    likeAnimationMap: {};
    liked: boolean;
    likedCount: number;
    medal: {
        detailPage: string
        wearPic: string
    };
    detailPage: string;
    wearPic: string;
    needDisplayTime: true;
    owner: false;
    parentCommentId: number;
    pendantData: {
        id: number;
        imageUrl: string;
    };
    repliedMark: null;
    richContent: null;
    showFloorComment: null;
    status: number;
    time: number;
    timeStr: string;
    user: {
        locationInfo: null;
        liveInfo: null;
        anonym: number;
        avatarUrl: string;
        avatarDetail: string;
        nickname: string;
        // ...其他属性
    };
    userBizLevels: null;
}
export type MvCommentType = {
    cnum: number
    code: number
    comments: Array<MvCommentListType>
    hotComments: Array<MvCommentListType>
    isMusician: boolean
    more: boolean
    moreHot: boolean
    topComments: any[]
    total: number
    userId: number
}
