// 主题切换
import { ref, watchEffect } from 'vue'
const theme = ref<string>(localStorage.getItem("_theme_") as string || "s_green")
watchEffect(() => {
    document.documentElement.dataset.theme = theme.value
    localStorage.setItem("_theme_", theme.value)
})
export function useTheme() {
    return {
        switchTheme(themeName: string) {
            theme.value = themeName
        }
    }
}