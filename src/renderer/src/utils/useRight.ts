import { ref, onMounted, onUnmounted } from 'vue'
import type { Ref } from 'vue'
export function useRight(container: Ref<HTMLDivElement>) {
    const x = ref(0)
    const y = ref(0)
    const visible = ref(false)
    function showMenu(e: MouseEvent) {
        e.preventDefault()
        // e.stopPropagation()
        x.value = e.clientX
        y.value = e.clientY
        visible.value = true
    }

    function closeMenu() {
        visible.value = false
    }
    onMounted(() => {
        container.value.addEventListener('contextmenu', showMenu, true)
        window.addEventListener('click', closeMenu, true)
        window.addEventListener('contextmenu', closeMenu, true)
    })
    onUnmounted(() => {
        // container.value.removeEventListener('contextmenu', showMenu, true)
        window.removeEventListener('click', closeMenu, true)
    })

    return {
        x,
        y,
        visible,
    }
}