import { useRecomSingStore } from "./modules/recomList"
import { useOnlyHomeSetStore } from "./modules/onlyHomeSet"
import { useNewMusicStore } from "./modules/newMusic"
import { useRecomMvStore } from "./modules/recomMv"
import { useCountriesCodeListStore } from "./modules/countriesCodeList"
import { useEveylSongStore } from "./modules/eveylSong"
import { useLoginStore } from "./modules/login"
import { useMusicUrlStore } from "./modules/musicUrl"
import { useHotSearchStore } from "./modules/searchHotDetail"
import { useKeyWordSearchStore } from "./modules/keyWordSearch"
import { useSingerInfoStore } from "./modules/singerInfo"
import { useArtistAlbumStore } from "./modules/artistAlbum"
import { ussLyricStore } from "./modules/lyric"
import { useUserSongListStore } from "./modules/userSongList"
import { useSongListDetailStore } from "./modules/SongListDetail"
import { useCommentPlaylistStore } from "./modules/commentPlaylist"
import { useRecomProgramStore } from "./modules/recomProgram"


export const useStore = () => {
    return {
        recSingList: useRecomSingStore(),
        onlyHomeSet: useOnlyHomeSetStore(),
        newMusic: useNewMusicStore(),
        recomMv: useRecomMvStore(),
        countriesCode: useCountriesCodeListStore(),
        eveylSong: useEveylSongStore(),
        login: useLoginStore(),
        musicUrlInfo: useMusicUrlStore(),
        hotSearch: useHotSearchStore(),
        keyWordSearch: useKeyWordSearchStore(),
        singerDetail: useSingerInfoStore(),
        artistAlbum: useArtistAlbumStore(),
        lyric: ussLyricStore(),
        userSongList: useUserSongListStore(),
        songListDetail: useSongListDetailStore(),
        commentPlaylist: useCommentPlaylistStore(),
        recomProgram: useRecomProgramStore()
    }
}