import { defineStore } from "pinia"
const { ipcRenderer } = require('electron')
import { ref } from "vue"
import type { recomProgramType } from "./type"
export const useRecomProgramStore = defineStore('recomProgram', () => {
    const recomProgramList = ref<recomProgramType["body"]["programs"]>([])
    const lookList = ref<recomProgramType["body"]["programs"]>([])
    const getRecomProgramList = async () => {
        const res: recomProgramType = await ipcRenderer.invoke('getDjProgram')
        recomProgramList.value = res.body.programs
        lookList.value = recomProgramList.value.splice(-5)
    }
    return {
        recomProgramList,
        lookList,
        getRecomProgramList
    }
})