interface NewMusicPublicCode {
    category: number,
    code: number
}
export interface songType {
    album: object | null,
    alias: Array<any>,
    artists: Array<any>,
    // 试听
    audition: boolean | null,
    bMusic: object | null,
    commentThreadId: string,
    copyFrom: string,
    copyright: number,
    copyrightId: number | string,
    // 彩铃
    crbt: null | boolean,
    dayPlays: number,
    disc: string,
    duration: number | string,
    fee: number,
    ftype: number,
    hMusic: object | null,
    hearTime: number | string,
    hrMusic: object | null,
    id: number | string,
    lMusic: object | null,
    mMusic: object | null,
    mark: number,
    mp3Url: string | null,
    mvid: number | string,
    name: string,
    no: number,
    noCopyrightRcmd: boolean | null,
    originCoverType: number,
    originSongSimpleData: object | null,
    playedNum: number,
    popularity: number,
    position: number,
    privilege: {
        cp: number,
        cs: boolean,
        dl: number,
        dlLevel: string,
        downloadMaxbr: number,
        fee: number,
        fl: number,
        flag: number,
        freeTrialPrivilege: {
            resConsumable: boolean,
            userConsumable: boolean
        },
        id: number,
        maxbr: number,
        payed: number,
        pl: number,
        playMaxbr: number,
        preSell: boolean,
        sp: number,
        st: number,
    } | null,
    ringtone: string,
    rtUrl: string | null,
    rtUrls: Array<any>,
    rtype: number,
    rurl: null | string,
    score: number,
    sing: string | null,
    single: number,
    sqMusic: object | null,
    starred: boolean,
    starredNum: number,
    status: number,
    transName: string | null,

}
export interface NewMusicList {
    alg: string,
    canDislike: boolean,
    copywriter: string | null,
    id: string | number,
    picUrl: string,
    song: songType | null,
    trackNumberUpdateTime: string | null,
    type: number,
    name: string
}
export interface newMusicType extends NewMusicPublicCode {
    body:{
        "result": Array<NewMusicList>
    }
}