// 最新音乐
import { defineStore } from 'pinia'
import { ref } from "vue"
import type { newMusicType } from './type'
const { ipcRenderer } = require('electron')
export const useNewMusicStore = defineStore("newMusic", () => {
    const newMusicList = ref<newMusicType['body']['result']>([])
    const podcList = ref<newMusicType['body']['result']>()
    const getNewMusic = async () => {
        const res: newMusicType = await ipcRenderer.invoke('getNewMusic')
        newMusicList.value = res.body.result
        podcList.value = newMusicList.value.splice(-6)
    }
    return {
        newMusicList,
        podcList,
        getNewMusic
    }
})