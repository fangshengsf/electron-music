// 关键词搜索
import { defineStore } from 'pinia'
import { ref } from 'vue'
const { ipcRenderer } = require('electron')
import type { keyWordSearchType } from './type'
import { ElMessage } from 'element-plus'
export const useKeyWordSearchStore = defineStore('keyWordSearch', () => {
    const SearchList = ref<keyWordSearchType['body']['result']['songs']>([])
    const songCount = ref(0)
    const getSearchList = async (keywords: string) => {
        let res: keyWordSearchType = await ipcRenderer.invoke('keywordsSearch', keywords)
        if (res.body.code !== 200) {
            ElMessage.warning(res.body.message)
            return
        } else {
            SearchList.value = res.body.result.songs
            songCount.value = res.body.result.songCount
        }

    }
    return {
        SearchList,
        songCount,
        getSearchList
    }
})