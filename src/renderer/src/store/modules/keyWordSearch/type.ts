import type { songInfo } from '../eveylSong/type'
export interface keyWordSearchType {
    status: number,
    cookie: Array<string>,
    body: {
        code: number
        message: string
        result: {
            songCount: number,
            searchQcReminder: any,
            songs: Array<songInfo>
        }
    }

}