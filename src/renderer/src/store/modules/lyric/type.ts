export interface lyricType {
    body: {
        lrc: {
            lyric: string
            version: number
        },
        yrc?: {
            lyric: string
            version: number
        },
        romalrc: {
            lyric: string
            version: number
        },
        yromalrc?: {
            lyric: string
            version: number
        }
    }
}