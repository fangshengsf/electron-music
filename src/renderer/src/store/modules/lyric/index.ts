// 歌词
import { defineStore } from 'pinia'
const { ipcRenderer } = require('electron')
import type { lyricType } from './type'
import { ref } from 'vue'
export const ussLyricStore = defineStore('lyric', () => {
    const lrc_lyricList = ref<Array<string>>([])
    // const yrc_lyricList = ref('')
    const getLyricList = async (id: number) => {
        const res: lyricType = await ipcRenderer.invoke('getLyric', id)
        res.body.lrc.lyric.split('\n').forEach((item) => {
            lrc_lyricList.value.push(item)
        })
    }

    return {
        lrc_lyricList,
        // yrc_lyricList,
        getLyricList
    }
})