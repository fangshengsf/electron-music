// 歌单评论
import { defineStore } from 'pinia'
import { ref } from 'vue'
const { ipcRenderer } = require('electron')
import type { commentPlaylistType } from './type'
import { ElMessage } from "element-plus"
export const useCommentPlaylistStore = defineStore('commentPlaylist', () => {
    const comments = ref<commentPlaylistType['body']['data']['comments']>([])
    const getCommentPlaylist = async (id: number | string, type = 2, pageNo = 1, pageSize = 20) => {
        await ipcRenderer.invoke('getCommentNew', id, type, pageNo, pageSize).then((res: commentPlaylistType) => {
            comments.value = res.body.data.comments
        }).catch(() => {
            ElMessage.warning('网络太拥挤，请稍候再试！')
        })
    }
    return {
        comments,
        getCommentPlaylist
    }
})