interface Infotype {
    "vipCode": number,
    "rights": boolean,
    "iconUrl": string
}
interface userType {
    "avatarDetail": {
        "userType": number,
        "identityLevel": number,
        "identityIconUrl": string
    } | null,
    "commonIdentity": null,
    "locationInfo": null,
    "liveInfo": null,
    "followed": boolean,
    "vipRights": {
        "associator": Infotype | null,
        "musicPackage": Infotype | null
        "redplus": Infotype | null
        "redVipAnnualCount": number,
        "redVipLevel": number,
        "relationType": number
    },
    "relationTag": null,
    "anonym": number,
    "encryptUserId": string,
    "userId": number,
    "userType": number,
    "nickname": string,
    "avatarUrl": string,
    "authStatus": number,
    "expertTags": null,
    "experts": null,
    "vipType": number,
    "remarkName": null,
    "isHug": boolean,
    "socialUserId": null,
    "target": null
}
interface commentsType {
    "user": userType,
    "beReplied": null,
    "commentId": number,
    "threadId": string,
    "content": string,
    "richContent": null,
    "status": number,
    "time": number,
    "timeStr": string,
    "needDisplayTime": boolean,
    "likedCount": number,
    "replyCount": number,
    "liked": boolean,
    "expressionUrl": null,
    "parentCommentId": number,
    "repliedMark": boolean,
    "pendantData": null,
    "pickInfo": null,
    "showFloorComment": {
        "replyCount": number,
        "comments": null,
        "showReplyCount": boolean,
        "topCommentIds": null,
        "target": null
    },
    "decoration": {},
    "commentLocationType": number,
    "musicianSayAirborne": null,
    "args": string
    "tag": {
        "datas": null,
        "extDatas": Array<any>,
        "contentDatas": null,
        "contentPicDatas": null,
        "relatedCommentIds": null
    },
    "source": null,
    "resourceSpecialType": null,
    "extInfo": {},
    "commentVideoVO": {
        "showCreationEntrance": boolean,
        "allowCreation": boolean,
        "creationOrpheusUrl": null,
        "playOrpheusUrl": null,
        "videoCount": number,
        "forbidCreationText": string
    },
    "contentResource": null,
    "contentPicNosKey": null,
    "contentPicUrl": null,
    "grade": null,
    "userBizLevels": null,
    "userNameplates": null,
    "ipLocation": {
        "ip": null,
        "location": "",
        "userId": null
    },
    "owner": boolean,
    "tail": null,
    "hideSerialComments": null,
    "hideSerialTips": null,
    "topicList": null,
    "privacy": number,
    "track": string
}
export interface commentPlaylistType {
    "body": {
        "code": number,
        "message": string,
        "data": {
            "comments": Array<commentsType>,
            "commentsTitle": string,
            "currentCommentTitle": string,
            "currentComment": null,
            "totalCount": number,
            "hasMore": true,
            "cursor": string,
            "sortType": number,
            "sortTypeList": Array<{
                "sortType": number,
                "sortTypeName": string,
                "target": string
            }>
            "style": string,
            "bottomAction": null,
            "likeAnimation": {
                "animationConfigMap": {
                    "EVENT_FEED": [],
                    "MOMENT": [],
                    "INPUT": [],
                    "COMMENT_AREA": []
                },
                "version": number
            }
        }
    }
    "status": number,
    "cookie": Array<string>

}