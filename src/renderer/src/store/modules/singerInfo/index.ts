// 歌手信息
import { defineStore } from 'pinia'
import { ref } from 'vue'
const { ipcRenderer } = require('electron')
import type { singerType, singerInfo } from '@renderer/store/modules/singerInfo/type'
export const useSingerInfoStore = defineStore('singerInfo', () => {
    const singerInfo = ref<singerType>()
    const getSingerDetail = async (id: string | number) => {
        let res: singerInfo = await ipcRenderer.invoke('getArtist_detail', id)
        singerInfo.value = res.body.data
    }
    return {
        singerInfo,
        getSingerDetail
    }
})