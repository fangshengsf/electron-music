interface artistType {
    albumSize: number;
    briefDesc: string;
    id: number;
    avatar: string;
    cover: string;
    identifyTag: any;
    identities: Array<any>;
    alias: Array<string>;
    musicSize: number;
    mvSize: number;
    name: string;
    rank: number | null;
    transNames: Array<any>;
}
interface secondaryExpertIdentiyType {
    expertIdentiyCount: number;
    expertIdentiyId: number;
    expertIdentiyName: string;
}
interface identifyType {
    actionUrl: string;
    imageDesc: string;
    imageUrl: string | null;
}

export interface singerType {
    artist: artistType;
    blacklist: boolean;
    preferShow: number;
    secondaryExpertIdentiy: Array<secondaryExpertIdentiyType>;
    showPriMsg: boolean;
    videoCount: number;
    identify?: identifyType;
}
export interface singerInfo {
    body: {
        code: number
        message: string
        data: singerType
    }
    cookie: Array<any>
    status: number

}