// 用户歌单
import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { UserSongListType } from "./type"
const { ipcRenderer } = require('electron')
export const useUserSongListStore = defineStore('userSongList', () => {
    const userSongList_playlist = ref<UserSongListType['body']['playlist']>([])
    const getUserSongList = async (uid: number | string) => {
        const res: UserSongListType = await ipcRenderer.invoke('getUserPlaylist', uid)
        userSongList_playlist.value = res.body.playlist
    }
    return {
        userSongList_playlist,
        getUserSongList
    }
})