export interface loginFrom {
    phone: string
    password: string,
    countrycode?: string | number,
    md5_password?: string,
    captcha?: string | number
    isOk?: boolean,
    autoLogin?: boolean
}
interface accountType {
    anonimousUser: boolean,
    ban: number,
    baoyueVersion: number,
    createTime: number | string,
    donateVersion: number | string,
    id: string | number,
    salt: string,
    status: string,
    tokenVersion: number | string,
    type: number | string,
    uninitialized: boolean,
    userName: string,
    vipType: number | string,
    viptypeVersion: number | string,
    whitelistAuthority: number
}
export interface profileType {
    "followed": boolean,
    "backgroundUrl": string,
    "backgroundImgIdStr": string,
    "userType": number | string,
    "avatarImgIdStr": string,
    "avatarUrl": string,
    "avatarImgId": number | string,
    "backgroundImgId": number | string,
    "vipType": number | string,
    "authStatus": number | string,
    "djStatus": number | string,
    "detailDescription": string,
    "experts": object | null,
    "expertTags": null | any,
    "accountStatus": number | string,
    "nickname": string,
    "birthday": number | string,
    "gender": number | string,
    "province": number | string,
    "city": number | string,
    "defaultAvatar": false,
    "mutual": false,
    "remarkName": null | any,
    "description": string,
    "userId": number | string,
    "signature": string,
    "authority": number | string,
    "followeds": number | string,
    "follows": number | string,
    "eventCount": number | string,
    "avatarDetail": null | any,
    "playlistCount": number | string,
    "playlistBeSubscribedCount": number | string
}
export interface userInfoType {
    account: accountType | null,
    bindings: Array<any>,
    code: number | string,
    cookie: string,
    loginType: number,
    profile: profileType,
    token: string,
    msg?: string,
    message?: string
}