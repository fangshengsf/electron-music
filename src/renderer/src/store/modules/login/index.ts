// 登陆
import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { userInfoType } from './type'
import { emailLogin } from "@renderer/api"
export const useLoginStore = defineStore('login', () => {
    const userInfo = ref<userInfoType['profile']>()
    const token = ref<string>('')
    const cookie = ref<string>('')
    const getUserInfo = async (email: string, password: string) => {
        const res = await emailLogin(email, password)
        userInfo.value = res.profile
        token.value = res.token
        cookie.value = res.cookie
        return res
    }
    return {
        userInfo,
        getUserInfo,
        token,
        cookie
    }
},
    {
        persist: {
            key: 'token&cookie',
            storage: localStorage,
            paths: ['token', 'cookie']
        }
    }
)