// 每日歌曲
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { eveylSongType } from './type'
const { ipcRenderer } = require('electron')
export const useEveylSongStore = defineStore('eveylSong', () => {
    const eveylSongList = ref<eveylSongType['body']['data']['dailySongs']>([])
    const getEveylSongList = async () => {
        const res: eveylSongType = await ipcRenderer.invoke('getEveylSongList')
        eveylSongList.value = res.body.data.dailySongs
    }
    return {
        eveylSongList,
        getEveylSongList,
    }
})