interface alType {
    "id": string | number,
    "name": string,
    "tns": Array<any> | null,
    "pic_str": string,
    "picUrl": string,
    "pic": string | number
}
interface arType {
    "id": string | number,
    "name": string,
    "tns": Array<any> | null,
    "alias": Array<any> | null,
}
interface hType {
    "br": number | string,
    "fid": number | string,
    "size": number | string,
    "vd": number | string,
    "sr": number | string
}
interface mType extends hType {

}
interface lType extends hType {

}
interface sqType extends hType {

}
interface hrType extends hType {

}
export interface videoType {
    "vid": string,
    "type": number | string,
    "title": string,
    "playTime": number | string,
    "coverUrl": string,
    "publishTime": number | string,
    "artists": null,
    "alias": null
}
interface videoInfoType {
    "moreThanOne": boolean,
    "video": null | videoType,
}
export interface PrivilegeType {
    id: number;
    fee: number;
    payed: number;
    realPayed: number;
    st: number;
    pl: number;
    dl: number;
    sp: number;
    cp: number;
    subp: number;
    cs: boolean;
    maxbr: number;
    fl: number;
    pc: null;
    toast: boolean;
    flag: number;
    paidBigBang: boolean;
    preSell: boolean;
    playMaxbr: number;
    downloadMaxbr: number;
    maxBrLevel: string;
    playMaxBrLevel: string;
    downloadMaxBrLevel: string;
    plLevel: string;
    dlLevel: string;
    flLevel: string;
    rscl: null;
    freeTrialPrivilege: {
        resConsumable: boolean;
        userConsumable: boolean;
        listenType: number | string;
        cannotListenReason: number | string;
        playReason: any;
    };
    rightSource: number;
    chargeInfoList: {
        rate: number;
        chargeUrl: null;
        chargeMessage: null;
        chargeType: number;
    }[];
};
interface songListType {
    "id": number | string,
    "name": string
}
interface originSongType {
    "songId": number | string,
    "name": string,
    "artists": Array<songListType>
    "albumMeta": songListType
}
export interface songInfo {
    name: string,
    id: number | string,
    pst: number,
    t: number,
    ar: Array<arType>,
    alia: Array<any> | null,
    pop: number,
    st: number,
    rt: "",
    fee: number,
    v: number,
    crbt: null,
    cf: string,
    al: alType,
    dt: number | string,
    h: hType,
    m: mType,
    l: lType,
    sq: sqType,
    hr: hrType,
    "a": null,
    "cd": string,
    "no": number,
    "rtUrl": null,
    "ftype": number,
    "rtUrls": Array<any> | null,
    "djId": number,
    "copyright": number,
    "s_id": number,
    "mark": number | string,
    "originCoverType": number,
    "originSongSimpleData": null | originSongType,
    "tagPicList": null,
    "resourceState": true,
    "version": number,
    "songJumpInfo": null,
    "entertainmentTags": null,
    "single": number,
    "noCopyrightRcmd": null,
    "rtype": number,
    "rurl": null,
    "mst": number,
    "cp": number | string,
    "mv": number,
    "publishTime": number | string,
    "reason": string,
    "videoInfo": null | videoInfoType,
    "tns"?: Array<string>,
    "recommendReason": string,
    "privilege": PrivilegeType | null,
    "alg": string
    "playing": false | true
}
export interface eveylSongType {
    body: {
        code: number | string,
        data: {
            dailySongs: Array<songInfo>,
        }
    }
}