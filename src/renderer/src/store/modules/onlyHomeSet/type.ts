export interface OnlyHomePublicCode {
    code: number,
    name: string
}
export interface OnlyHomeSetList {
    alg: string,
    copywriter: string,
    id: string | number,
    name: string,
    picUrl: string,
    sPicUrl: string,
    type: number,
    url: string,
}
export interface OnlyHomeType extends OnlyHomePublicCode {
    body:{
        "result": OnlyHomeSetList[]
    }
}