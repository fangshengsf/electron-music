// 独家放送
import { defineStore } from 'pinia'
import { ref } from 'vue'
const { ipcRenderer } = require('electron')
import type { OnlyHomeType } from './type'
export const useOnlyHomeSetStore = defineStore('onlyHomeSet', () => {
    const onlyHomeSetList = ref<OnlyHomeType['body']['result']>([])
    const getOnlyHomeSet = async () => {
        const res: OnlyHomeType = await ipcRenderer.invoke('getOnlyHomeSet')
        onlyHomeSetList.value = res.body.result
    }
    return {
        onlyHomeSetList,
        getOnlyHomeSet
    }
}) 