// 国家编码列表
import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import type { countriesCodeLisType, countryListType } from './type'
const { ipcRenderer } = require('electron')
export const useCountriesCodeListStore = defineStore('countriesCodeList', () => {
    const countriesCodeList = ref<countriesCodeLisType['body']['data']>([])
    const countryList = computed(() => {
        let ls: Array<countryListType> = [];
        countriesCodeList.value.forEach(item => {
            item.countryList.forEach(country => {
                ls.push(country)
            })
        })
        return ls
    })
    const getContriesCodeList = async () => {
        let res: countriesCodeLisType = await ipcRenderer.invoke('getCountriesCodeList')
        countriesCodeList.value = res.body.data
    }
    return {
        countriesCodeList,
        getContriesCodeList,
        countryList
    }
})