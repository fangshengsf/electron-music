export interface countriesCodeListPublicType {
    code: number | string,
    message: string
}

export interface countryListType {
    zh: string,
    en: string,
    code: string | number,
    locale: string
}
export interface countriesCodeList {
    label: string,
    countryList: Array<countryListType>
}
export interface countriesCodeLisType {
    body: countriesCodeListPublicType & {
        data: countriesCodeList[]
    }
}