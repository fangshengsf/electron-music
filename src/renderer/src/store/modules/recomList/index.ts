// 推荐歌单
import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { ResultType, singList } from './type'
const { ipcRenderer } = require('electron')
export const useRecomSingStore = defineStore('recommountSingList', () => {
    const recomSingList = ref<singList[]>([])
    const bookList = ref<singList[]>([])
    const getRecomSingList = async () => {
        const res: ResultType = await ipcRenderer.invoke('getRecomSingList')
        recomSingList.value = res.body.result
        bookList.value = recomSingList.value.splice(-5)
    }
    return {
        recomSingList,
        bookList,
        getRecomSingList
    }
})