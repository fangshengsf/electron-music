interface ResPublicCode {
    category: number
    code: number
    hasTaste: boolean
}
export interface singList {
    "id": number | string,
    "type": number,
    "name": string,
    "copywriter": string,
    "picUrl": string,
    "canDislike": boolean,
    "trackNumberUpdateTime": number,
    "playCount": number,
    "trackCount": number,
    "highQuality": boolean,
    "alg": string
}
export interface ResultType extends ResPublicCode {
    "body": {
        "result": singList[]
    }


}