export interface hotDetailPublicCode {
    "code": number,
    "message": string,
}
export interface hotDetailType {
    "searchWord": string,
    "score": number,
    "content": string,
    "source": number,
    "iconType": number,
    "iconUrl": string | null,
    "url": string,
    "alg": string
}
export interface hotDetail {
    body: hotDetailPublicCode & {
        "data": Array<hotDetailType>
    }
}