// 热搜列表
import { defineStore } from 'pinia'
import { ref } from 'vue'
const { ipcRenderer } = require('electron')
import type { hotDetail } from './type'
export const useHotSearchStore = defineStore('hotDetailList', () => {
    const hotSearchList = ref<hotDetail['body']['data']>([])
    const getHotSearchList = async () => {
        const res: hotDetail = await ipcRenderer.invoke('searchHotDetail')
        hotSearchList.value = res.body.data
    }
    return {
        hotSearchList,
        getHotSearchList
    }
})