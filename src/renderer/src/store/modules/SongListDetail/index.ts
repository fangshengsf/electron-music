// 歌单详情
import { defineStore } from "pinia"
const { ipcRenderer } = require('electron')
import type { songListDetailType } from './type'
import { ref } from "vue"
export const useSongListDetailStore = defineStore('songListDetail', () => {
    const songListDetailInfo = ref<songListDetailType['body']>()
    const tracks = ref<songListDetailType['body']['playlist']['tracks']>([])
    const getSongListDetail = async (id: number | string) => {
        const res: songListDetailType = await ipcRenderer.invoke('getSonglistDetail', id)
        songListDetailInfo.value = res.body
        tracks.value = res.body.playlist.tracks
    }
    return {
        songListDetailInfo,
        tracks,
        getSongListDetail
    }
})