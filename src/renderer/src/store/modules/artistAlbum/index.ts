// 歌手专辑
import { defineStore } from 'pinia'
import type { artistAlbum } from './type'
import { ref } from 'vue'
const { ipcRenderer } = require('electron')
export const useArtistAlbumStore = defineStore('artistAlbum', () => {
    const artistAlbumList = ref<artistAlbum['body']['hotAlbums']>([])
    const getArtistAlbum = async (id: number | string) => {
        let res: artistAlbum = await ipcRenderer.invoke('getArtistAlbum', id)
        artistAlbumList.value = res.body.hotAlbums
    }
    return {
        artistAlbumList,
        getArtistAlbum
    }
})