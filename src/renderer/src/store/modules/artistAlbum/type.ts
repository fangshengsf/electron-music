export interface hotAlbumsType {
    "songs": Array<any>,
    "paid": boolean,
    "onSale": boolean,
    "mark": number,
    "awardTags": null,
    "artists": Array<artistsType>,
    "copyrightId": number,
    "picId": number,
    "artist": artistsType,
    "publishTime": number,
    "company": string,
    "briefDesc": string,
    "picUrl": string,
    "commentThreadId": string,
    "blurPicUrl": string,
    "companyId": number,
    "pic": number,
    "status": number,
    "subType": string,
    "alias": Array<any>,
    "description": string,
    "tags": string,
    "name": string,
    "id": number,
    "type": string,
    "size": number,
    "picId_str": string,
    "isSub": boolean
}
interface artistsType {
    "img1v1Id": number,
    "topicPerson": number,
    "picId": number,
    "briefDesc": string,
    "musicSize": number,
    "albumSize": number,
    "picUrl": string,
    "followed": boolean,
    "img1v1Url": string,
    "trans": string,
    "alias": Array<any>,
    "name": string,
    "id": number,
    "img1v1Id_str": string,
    "picId_str"?: string
}
export interface artistAlbum {
    body: {
        artist:Object,
        hotAlbums:Array<hotAlbumsType>
    },
    cookie: Array<any>,
    status: number
}