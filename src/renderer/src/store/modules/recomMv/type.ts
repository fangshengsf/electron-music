export interface recomMvPublicCode {
    category: number
    code: number | string
}
export interface artistsType {
    id: number | string,
    name: string
}
export interface recomMvList {
    id: number | string,
    name: string,
    type: string | number,
    copywriter: string,
    picUrl: string,
    canDislike: boolean,
    trackNumberUpdateTime: string | null,
    duration: number,
    playCount: number,
    subed: boolean,
    artists: artistsType[],
    artistName: string,
    artistId: number | string,
    alg: string
}
export interface recomMvType {
    "body":{
        "result": recomMvList[]
    }
}