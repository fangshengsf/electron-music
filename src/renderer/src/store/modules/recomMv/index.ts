// 推荐MV
import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { recomMvType } from './type'
const { ipcRenderer } = require('electron')
export const useRecomMvStore = defineStore('recomMv', () => {
    const recomMvList = ref<recomMvType["body"]["result"]>([])
    const getRecomMvList = async () => {
        const res: recomMvType = await ipcRenderer.invoke('getRecomMvList')
        recomMvList.value = res.body.result
    }
    return {
        recomMvList,
        getRecomMvList
    }
})