interface freeTrialPrivilegeType {
    "resConsumable": boolean,
    "userConsumable": boolean,
    "type": string | number,
    "remainTime": string | number
}
interface freeTimeTrialPrivilegeType {
    "resConsumable": boolean,
    "userConsumable": boolean,
    "listenType": null | object,
    "cannotListenReason": null | object,
    "playReason": null | object
}
export interface musicInfoType {
    "id": number | string,
    "url": string,
    "br": string | number,
    "size": string | number,
    "md5": string,
    "code": string | number,
    "expi": string | number,
    "type": string,
    "gain": number,
    "peak": string | number,
    "fee": string | number,
    "uf": null,
    "payed": string | number,
    "flag": string | number,
    "canExtend": boolean,
    "freeTrialInfo": null | object,
    "level": string,
    "encodeType": string,
    "channelLayout": null | object,
    "freeTrialPrivilege": freeTrialPrivilegeType | null
    "freeTimeTrialPrivilege": freeTimeTrialPrivilegeType | null,
    "urlSource": string | number,
    "rightSource": string | number,
    "podcastCtrp": null | object,
    "effectTypes": null | object,
    "time": string | number
}
export interface musicUrlType {
    body: {
        code: number
        data: Array<musicInfoType>
        message?: string
    }
    status: number
    cookie: Array<string>
}