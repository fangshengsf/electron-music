// 歌曲播放url
import { defineStore } from 'pinia'
import { ref } from 'vue'
const { ipcRenderer } = require('electron')
import type { musicUrlType } from './type'
export const useMusicUrlStore = defineStore('musicUrl', () => {
    const musicUrl = ref<musicUrlType['body']['data']>([])
    const setMusicUrl = async (id: string | number) => {
        let res: musicUrlType = await ipcRenderer.invoke('getSong_Url', id)
        if (!res.body.data[0].url) {
            return false
        }
        musicUrl.value = res.body.data
        return true
    }
    return {
        musicUrl,
        setMusicUrl
    }
})