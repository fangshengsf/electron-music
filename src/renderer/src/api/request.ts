import axios from "axios"
const requests = axios.create({
    baseURL: "/api",
    timeout: 5000,
    withCredentials: true
})
requests.interceptors.request.use(config => {
    // config.headers.Authorization = JSON.parse(localStorage.getItem("token&cookie") as string).token
    // config.headers.Cookie = JSON.parse(localStorage.getItem("token&cookie") as string).cookie
    return config
}
)
requests.interceptors.response.use((res) => {
    return res.data
}, (err) => {
    return Promise.reject(err)
})
export default requests