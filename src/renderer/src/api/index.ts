import requests from "./request";
import type { userInfoType } from "../store/modules/login/type"
// 邮箱登录
export const emailLogin = (email: string, password: string) => requests<{ email: string, password: string }, userInfoType>({ url: `/login?email=${email}&password=${password}`, method: "get" })