import { createApp } from 'vue'
import App from './App.vue'
import './assets/css/main.css'
import './assets/css/theme.css'
import './assets/fonts/iconfont.css'
// 引入Element
import 'element-plus/dist/index.css'
// 初始化创建仓库
import { createPinia } from 'pinia'
// 持久化存储
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import registerDirectives from "@renderer/directives"
// 全局事件总线
import mitt from 'mitt'
const bus = mitt()
const store = createPinia()
store.use(piniaPluginPersistedstate)
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true'
import router from './router'
export const app = createApp(App)
app.use(store)
app.use(router)
registerDirectives(app)
app.mount('#app')
// 注册全局事件总线
app.config.globalProperties.$bus = bus
