export default {
    mounted: (el: HTMLElement) => {
        el.addEventListener("mousedown", (e: MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            const disX = e.clientX - el.offsetLeft;
            const disY = e.clientY - el.offsetTop;

            document.onmousemove = (e: MouseEvent) => {
                let left = e.clientX - disX;
                let top = e.clientY - disY;

                // 获取body元素的宽度和高度
                const bodyWidth = document.body.offsetWidth;
                const bodyHeight = document.body.offsetHeight;

                // 获取元素的宽度和高度
                const elementWidth = el.offsetWidth;
                const elementHeight = el.offsetHeight;

                // 设置边界限制
                if (left < 0) {
                    left = 0;
                } else if (left + elementWidth > bodyWidth) {
                    left = bodyWidth - elementWidth;
                }

                if (top < 0) {
                    top = 0;
                } else if (top + elementHeight > bodyHeight) {
                    top = bodyHeight - elementHeight;
                }

                el.style.left = left + "px";
                el.style.top = top + "px";
            };

            document.onmouseup = () => {
                document.onmousemove = null;
                document.onmouseup = null;
            };
        });
    }
}
