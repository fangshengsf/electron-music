import type { App } from "vue";
import sizeDirect from "./sizeDirect";
import draggable from "./draggable";
export default (app: App) => {
    app.directive("sizeDirect", sizeDirect)
    app.directive("draggable", draggable)
}