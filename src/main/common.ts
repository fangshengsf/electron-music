import { ipcMain } from 'electron'
import { login_cellphone, personalized, program_recommend, personalized_privatecontent, personalized_newsong, personalized_mv, countries_code_list, recommend_songs, logout, search_hot_detail, search_default, artist_album, cloudsearch, song_url_v1, artist_detail, lyric_new, user_playlist, playlist_detail, comment_new, banner, song_detail, search_suggest, mv_url, mv_detail, mv_detail_info, comment_mv, artist_mv, simi_artist, album, comment_album } from 'NeteaseCloudMusicApi'
export const apiList = () => {
    // 手机号登录
    // ipcMain.handle('phoneLogin', async (_, phone: string, password: string) => {
    //     return await login_cellphone({ phone, password, realIP: '40.125.128.0' })
    // })
    // 获取推荐歌单
    ipcMain.handle('getRecomSingList', async () => {
        return await personalized({ limit: 15 })
    })
    // 获取独家放送
    ipcMain.handle('getOnlyHomeSet', async () => {
        return await personalized_privatecontent({})
    })
    // 获取最新音乐
    ipcMain.handle("getNewMusic", async () => {
        return await personalized_newsong({ limit: 18 })
    })
    // 关键词搜索
    ipcMain.handle("keywordsSearch", async (_, keywords: string) => {
        return await cloudsearch({ keywords })
    })
    // 获取推荐mv
    ipcMain.handle("getRecomMvList", async () => {
        return await personalized_mv({})
    })
    // 获取手机号的国家编码
    ipcMain.handle("getCountriesCodeList", async () => {
        return await countries_code_list({})
    })
    // 获取每日30首
    ipcMain.handle("getEveylSongList", async () => {
        return await recommend_songs({})
    })
    // 退出登录
    ipcMain.handle("logout", async () => {
        return await logout({})
    })
    // 搜索热搜
    ipcMain.handle("searchHotDetail", async () => {
        return await search_hot_detail({})
    })
    // 默认搜索
    ipcMain.handle("searchDefault", async () => {
        return await search_default({})
    })
    // 获取音乐url
    ipcMain.handle("getSong_Url", async (_, id: number | string, level = "exhigh") => {
        return await song_url_v1({ id, level })
    })
    // 获取歌手专辑信息
    ipcMain.handle("getArtistAlbum", async (_, id: number | string) => {
        return await artist_album({ id, limit: 40 })
    })
    // 获取歌手信息
    ipcMain.handle("getArtist_detail", async (_, id: number | string) => {
        return await artist_detail({ id })
    })
    // 获取歌手MV
    ipcMain.handle("getArtist_mv", async (_, id: number | string) => {
        let { body } = await artist_mv({ id })
        return body.mvs
    })
    // 获取相似歌手
    ipcMain.handle("getSimi_artist", async (_, id: number | string) => {
        let { body } = await simi_artist({ id })
        return body.artists
    })
    // 获取音乐歌词
    ipcMain.handle("getLyric", async (_, id: number | string) => {
        return await lyric_new({ id })
    })
    // 获取用户歌单
    ipcMain.handle("getUserPlaylist", async (_, uid: number | string) => {
        return await user_playlist({ uid })
    })
    // 获取歌单详情
    ipcMain.handle("getSonglistDetail", async (_, id: number | string) => {
        return await playlist_detail({ id })
    })
    // 获取歌单评论
    ipcMain.handle("getCommentNew", async (_, id: number | string, type: number, pageNo: number, pageSize: number) => {
        return await comment_new({ id, type, pageNo, pageSize })
    })
    // 获取轮播图
    ipcMain.handle("getBanner", async () => {
        let { body } = await banner({})
        return body
    })
    // 获取歌曲详情
    ipcMain.handle("getSongDetail", async (_, ids: string) => {
        let { body } = await song_detail({ ids })
        return body.songs
    })
    // 获取专辑详情
    ipcMain.handle("getAlbumDetail", async (_, id: number | string) => {
        let { body } = await album({ id })
        return body
    })
    // 获取专辑评论
    ipcMain.handle("getAlbumComment", async (_, id: number | string, offset: number) => {
        let { body } = await comment_album({ id, offset })
        return body
    })
    // 获取推荐节目
    ipcMain.handle("getDjProgram", async () => {
        return await program_recommend({ type: "" })
    })
    // 获取mv播放地址
    ipcMain.handle("getMvUrl", async (_, id: number | string) => {
        let { body } = await mv_url({ id })
        return body.data
    })
    // 获取mv信息
    ipcMain.handle("getMvInfo", async (_, mvid: number | string) => {
        let { body } = await mv_detail({ mvid })
        return body.data
    })
    // 获取mv点赞转发评论数数据
    ipcMain.handle("getMvDetail", async (_, mvid: number | string) => {
        let { body } = await mv_detail_info({ mvid })
        return body
    })
    // 获取mv评论
    ipcMain.handle("getMvComment", async (_, id: number | string, offset: number) => {
        let { body } = await comment_mv({ id, offset })
        return body
    })
    // 搜索建议
    ipcMain.handle("searchSuggest", async (_, keywords: string) => {
        let { body } = await search_suggest({ keywords })
        return body
    })
}
